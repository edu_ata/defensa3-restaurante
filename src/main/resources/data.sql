delete from category;
delete from restaurant;
delete from city;

INSERT INTO category(id, name) VALUES (1000, 'Desayuno');
INSERT INTO category(id, name) VALUES (1001, 'Almuerzo');
INSERT INTO category(id, name) VALUES (1002, 'Cena Familiar');

INSERT INTO city(id, name) VALUES (1000, 'Cochabamba');
INSERT INTO city(id, name) VALUES (1001, 'La Paz');
INSERT INTO city(id, name) VALUES (1002, 'Sucre');

INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion,latitud,longitud) VALUES (1001, 'HyperMaxi',1000,1000,'buffet','foto1','cbba',-17.3784403,-66.1484966);
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1002, 'IceNorte',1001,1001,'almuerzos','foto2','cbba');
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1003, 'Planchitas',1002,1002,'platos variados','foto3','cbba');
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1004, 'Pizzeria Malcriada ',1002,1002,'platos variados','foto3','cbba');
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1005, 'Los 4 Toros',1002,1000,'platos variados','foto3','cbba');
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1006, 'Jakaranda',1002,1001,'platos variados','foto3','cbba');
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1007, 'El palacio del Sillapncho',1002,1002,'platos variados','foto3','cbba');
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1008, 'Panchita',1002,1000,'platos variados','foto3','cbba');
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1009, 'Pollos KFC',1002,1001,'platos variados','foto3','cbba');
INSERT INTO restaurant(id, name,category_id,city_id,descripcion,foto,ubicacion) VALUES (1010, 'Kingdong',1002,1002,'platos variados','foto3','cbba');
