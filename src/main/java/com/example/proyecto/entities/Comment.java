package com.example.proyecto.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity

public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String text;

    @ManyToOne
    @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;
/*
    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
*/
     @NotNull
     @Column(columnDefinition="int(11) default 0")
     private Integer fivestar=0;

    @NotNull
    @Column(columnDefinition="int(11) default 0")
    private Integer fourstar=0;

    @NotNull
    @Column(columnDefinition="int(11) default 0")
    private Integer treestar=0;

    @NotNull
    @Column(columnDefinition="int(11) default 0")
    private Integer twostar=0;

    @NotNull
    @Column(columnDefinition="int(11) default 0")
    private Integer onestar=0;

    public Integer getFivestar() {
        return fivestar;
    }

    public void setFivestar(Integer fivestar) {
        this.fivestar = fivestar;
    }

    public Integer getFourstar() {
        return fourstar;
    }

    public void setFourstar(Integer fourstar) {
        this.fourstar = fourstar;
    }

    public Integer getTreestar() {
        return treestar;
    }

    public void setTreestar(Integer treestar) {
        this.treestar = treestar;
    }

    public Integer getTwostar() {
        return twostar;
    }

    public void setTwostar(Integer twostar) {
        this.twostar = twostar;
    }

    public Integer getOnestar() {
        return onestar;
    }

    public void setOnestar(Integer onestar) {
        this.onestar = onestar;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
/*
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
*/
}
