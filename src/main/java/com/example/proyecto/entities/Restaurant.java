package com.example.proyecto.entities;

import org.hibernate.annotations.ColumnDefault;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String name;

    @NotNull
    private String descripcion;

    @NotNull
    private String ubicacion;

    @NotNull
    private String foto;


    private Float longitud;


    private Float latitud;

    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL)
    List<Comment> comments;


    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private City city;

    @OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL)
    List<Likes> likes;

    private int cont_comentarios;

    public Restaurant (){
        this.cont_comentarios=0;
    }


/*
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

  */

    public List<Likes> getLikes(){
        return likes;
    }

    public void setLikes(List<Likes> likes){
        this.likes=likes;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Float getLatitud() {
        return latitud;
    }

    public void setLatitud(Float latitud) {
        this.latitud = latitud;
    }

    public Float getLongitud() {
        return longitud;
    }

    public void setLongitud(Float longitud) {
        this.longitud = longitud;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public int getCont_comentarios() {
        return cont_comentarios;
    }

    public void setCont_comentarios(int cont_comentarios) {
        this.cont_comentarios = cont_comentarios;
    }

    /*
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    */

}