package com.example.proyecto.services;

import com.example.proyecto.entities.Comment;
import com.example.proyecto.entities.Restaurant;

public interface CommentService {

        Iterable<Comment> listAllComments();

        void saveComment(Comment comment);

        Comment getComment(Integer id);

        void deleteComment(Integer id);


}
