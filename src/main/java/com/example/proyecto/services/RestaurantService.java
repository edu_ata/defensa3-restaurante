package com.example.proyecto.services;
import com.example.proyecto.entities.Restaurant;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RestaurantService {

    Iterable<Restaurant> listAllRestaurants();
    void saveRestaurant(Restaurant restaurant);
    Restaurant getRestaurant(Integer id);
    void deleteRestaurant(Integer id);

    Iterable<Restaurant> getRestaurantsLikeName(String name);

    Iterable<Restaurant> getRestaurantsLikeCategory(Integer category);

    Iterable<Restaurant> getRestaurantsLikeCity(Integer city);

    Restaurant getTopRestaurantComent(Iterable<Restaurant> r);
}
