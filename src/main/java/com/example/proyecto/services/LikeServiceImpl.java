
package com.example.proyecto.services;


import com.example.proyecto.entities.Likes;
import com.example.proyecto.repository.LikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class LikeServiceImpl implements LikeService{
    private LikeRepository likeRepository;
    @Autowired
    @Qualifier(value = "likeRepository")
    public void setLikeRepository(LikeRepository likeRepository) {
        this.likeRepository = likeRepository;
    }

    @Override
    public Iterable<Likes> listAllLikes() {
        return likeRepository.findAll();
    }

    @Override
    public void saveLike(Likes likes) {
        likeRepository.save(likes);
    }



    @Override
    public Likes getLike(Integer id) {
        return likeRepository.findById(id).get();
    }

    @Override
    public void deleteLike(Integer id) {
        likeRepository.deleteById(id);;
    }
}


