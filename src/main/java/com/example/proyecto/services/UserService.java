package com.example.proyecto.services;

import com.example.proyecto.entities.User;
public interface UserService {

    void save(User user);
    User findByUsername(String username);

    Iterable<User> listAllUsers();

}
