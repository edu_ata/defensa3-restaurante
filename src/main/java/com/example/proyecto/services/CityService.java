package com.example.proyecto.services;

import com.example.proyecto.entities.City;

public interface CityService {
    Iterable<City> listAllCitys();

    void saveCity(City city);

    City getCity(Integer id);

    void deleteCity(Integer id);
}
