package com.example.proyecto.services;

import com.example.proyecto.entities.Restaurant;
import com.example.proyecto.repository.RestaurantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantServiceImpl implements RestaurantService {
   private RestaurantRepository restaurantRepository;
     @Autowired
     @Qualifier(value = "restaurantRepository")
     public void setRestaurantRepository(RestaurantRepository restaurantRepository) {
                this.restaurantRepository = restaurantRepository;
            }

     @Override
     public Iterable<Restaurant> listAllRestaurants() {
               return restaurantRepository.findAll();
            }

     @Override
     public void saveRestaurant(Restaurant restaurant){
         restaurantRepository.save(restaurant);
     }
     @Override
     public Restaurant getRestaurant(Integer id){
         return restaurantRepository.findById(id).get();
     }

     @Override
     public void deleteRestaurant(Integer id){
         restaurantRepository.deleteById(id);;
     }

    @Override
    public Iterable<Restaurant> getRestaurantsLikeName(String name) {

         return restaurantRepository.getRestaurantsLikeName(name);
    }

    @Override
    public Iterable<Restaurant> getRestaurantsLikeCategory(Integer category) {
        return restaurantRepository.getRestaurantsLikeCategory(category);
    }

    @Override
    public Iterable<Restaurant> getRestaurantsLikeCity(Integer city) {
        return restaurantRepository.getRestaurantsLikeCity(city);
    }

    @Override
    public Restaurant getTopRestaurantComent(Iterable<Restaurant> r) {

         //la idea era hacer que esta funcion devuelva el restaurante r con mayor contador
         return null;
    }


}
