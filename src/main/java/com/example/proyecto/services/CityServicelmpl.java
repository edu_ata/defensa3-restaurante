package com.example.proyecto.services;

import com.example.proyecto.entities.City;
import com.example.proyecto.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class CityServicelmpl implements CityService{

    private CityRepository cityRepository;

    @Autowired
    @Qualifier(value="cityRepository")
    public void setCityRepository(CityRepository cityRepository){
        this.cityRepository=cityRepository;
    }

    @Override
    public Iterable<City> listAllCitys(){
        return  cityRepository.findAll();
    }
    @Override
    public void saveCity(City city){
        cityRepository.save(city);
    }

    @Override
    public City getCity(Integer id){
        return cityRepository.findById(id).get();
    }
    @Override
    public void deleteCity(Integer id){
        cityRepository.deleteById(id);;
    }

}
