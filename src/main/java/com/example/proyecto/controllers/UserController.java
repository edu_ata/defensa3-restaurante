package com.example.proyecto.controllers;

import com.example.proyecto.entities.Category;
import com.example.proyecto.entities.City;
import com.example.proyecto.entities.Restaurant;
import com.example.proyecto.entities.User;
import com.example.proyecto.services.CategoryService;
import com.example.proyecto.services.CityService;
import com.example.proyecto.services.RestaurantService;
import com.example.proyecto.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.Null;
import java.util.List;

@Controller
public class UserController {


    @Autowired
    private UserService userService;
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CityService cityService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    public void setRestaurantService(RestaurantService restaurantService) { this.restaurantService = restaurantService; }
    public void setCategoryService(CategoryService categoryService) { this.categoryService = categoryService; }
    public void setCityService(CityService cityService) { this.cityService = cityService; }


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationInit(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("user") User user, BindingResult bindingResult, Model model) {
        ///userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "register";
        }
        userService.save(user);
        return "redirect:/";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String welcome(@RequestParam(value = "name", required = false, defaultValue="") String name,
                          //@RequestParam(value = "category_id", required = false, defaultValue= "") Integer category_id,
                          @RequestParam(value = "city_id", required = false, defaultValue= "") Integer city_id,
                          Model model, String logout) {


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User u = (org.springframework.security.core.userdetails.User)auth.getPrincipal();
        com.example.proyecto.entities.User user = userService.findByUsername(u.getUsername());
        model.addAttribute("user",user);



        Iterable<Restaurant> restaurantList =restaurantService.listAllRestaurants();
        Iterable<Restaurant> restaurantListSearch = null;
        Iterable<Restaurant> restaurantListSearch1 = null;
        Iterable<Category> categories= categoryService.listAllCategorys();
        model.addAttribute("categories",categories);
        Iterable<City> cities=cityService.listAllCitys();
        model.addAttribute("cities",cities);

        /*if(name.equals("")){
            restaurantList =restaurantService.listAllRestaurants();

        }else{*/
            //System.out.println("filtro" +name);

            if (name!=""){
                restaurantListSearch =restaurantService.getRestaurantsLikeName(name);
            }
            //if (category_id!=null){
                //restaurantListSearch1=restaurantService.getRestaurantsLikeCategory(category_id);
            //}
            else {
                if (city_id!=null){
                    //restaurantListSearch1=restaurantService.getRestaurantsLikeCity(city_id);
                }
            }



        //}

        model.addAttribute("restaurantList",restaurantList);
        model.addAttribute("restaurantListSearch",restaurantListSearch);
        //model.addAttribute("restaurantListSearch1",restaurantListSearch1);

        return "index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        return "login";
    }


    @RequestMapping(value = "/buscarName/{name}")
    String buscarByName(@PathVariable String name, Model model) {
        Iterable<Restaurant> restaurantListSearch=restaurantService.getRestaurantsLikeName(name);
        model.addAttribute("restaurantListSearch",restaurantListSearch);
        return "buscar";
    }

    @RequestMapping(value = "/buscarCategory/{category_id}")
    String buscarByCategory(@PathVariable Integer category_id, Model model) {

        Iterable<Restaurant> restaurantListSearch1=restaurantService.getRestaurantsLikeCategory(category_id);
        model.addAttribute("restaurantListSearch1",restaurantListSearch1);
        return "index";
    }

    @RequestMapping(value = "/buscarCity/{city_id}")
    String buscarByCity(@PathVariable Integer city_id, Model model) {

        Iterable<Restaurant> restaurantListSearch=restaurantService.getRestaurantsLikeCity(city_id);
        model.addAttribute("restaurantListSearch",restaurantListSearch);
        return "buscar";
    }



}
