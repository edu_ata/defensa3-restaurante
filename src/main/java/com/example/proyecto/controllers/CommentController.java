package com.example.proyecto.controllers;
import com.example.proyecto.entities.Comment;
import com.example.proyecto.entities.User;
import com.example.proyecto.entities.Restaurant;
import com.example.proyecto.services.CommentService;
import com.example.proyecto.services.CommentServiceImpl;
import com.example.proyecto.services.RestaurantService;
import com.example.proyecto.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class CommentController {
        private CommentService commentService;
        private RestaurantService restaurantService;

        @Autowired
        public void setCommentService(CommentService commentService) {
                this.commentService = commentService;
        }
        @Autowired
        public void setRestaurantService(RestaurantService restaurantService) {
                this.restaurantService = restaurantService;
        }

        @RequestMapping(value = "/comment", method = RequestMethod.POST)
        String save(Comment comment) {
                commentService.saveComment(comment);
                Restaurant r=comment.getRestaurant();
                System.out.println(r.getName());
                r.setCont_comentarios(r.getCont_comentarios()+1);
                restaurantService.saveRestaurant(r);
                return "redirect:/restaurant/"+comment.getRestaurant().getId();
            }

        @RequestMapping(value = "/ComentRestaurant")
        String topComentarioRestaurant(Model model){
                Restaurant restaurantTop=restaurantService.getTopRestaurantComent(restaurantService.listAllRestaurants());
                model.addAttribute("restaurantTop",restaurantTop);
                return "index";
        }



        /*
        @RequestMapping(value = "/buscarCity/{city_id}")
        String buscarByCity(@PathVariable Integer city_id, Model model) {

                Iterable<Restaurant> restaurantListSearch=restaurantService.getRestaurantsLikeCity(city_id);
                model.addAttribute("restaurantListSearch",restaurantListSearch);
                return "buscar";
            }
         */

}
