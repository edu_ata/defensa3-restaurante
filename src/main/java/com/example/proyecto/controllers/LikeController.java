package com.example.proyecto.controllers;

import com.example.proyecto.entities.Likes;
import com.example.proyecto.entities.User;
import com.example.proyecto.entities.Restaurant;
//import com.example.proyecto.services.LikeService;
//import com.example.proyecto.services.LikeServiceImpl;
import com.example.proyecto.services.LikeService;
import com.example.proyecto.services.RestaurantService;
import com.example.proyecto.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LikeController {

    private LikeService likeService;

    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    public void setLikeService(LikeService likeService) {
        this.likeService = likeService;
    }
    public void setRestaurantService(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @RequestMapping(value = "/like", method = RequestMethod.POST)
    String save(Likes like) {
        likeService.saveLike(like);
        return "redirect:/restaurant/"+like.getRestaurant().getId();
    }

    @RequestMapping(value ="/restaurantlike/{id}/{idrestaurant}",method = RequestMethod.GET)
    public String like(@PathVariable Integer id, @PathVariable Integer idrestaurant, Model model) {

        Restaurant restaurant=restaurantService.getRestaurant(idrestaurant);
        Likes likes=likeService.getLike(id);
        likes.setLikes(likes.getLikes()+1);
        likeService.saveLike(likes);

        return "redirect:/restaurant/"+restaurant.getId();

    }


}
