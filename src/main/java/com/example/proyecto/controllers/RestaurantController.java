package com.example.proyecto.controllers;
import com.example.proyecto.entities.*;
import com.example.proyecto.services.*;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


@Controller
public class RestaurantController {
    private RestaurantService restaurantService;
    private CategoryService categoryService;
    private CityService cityService;
    private UserService userService;
    //private LikeService likeService;

    @Autowired
    public void setRestaurantService(RestaurantService productService){
     this.restaurantService=productService;
    }
    @Autowired
    public void setCategoryService(CategoryService categoryService){
        this.categoryService=categoryService;
    }
    @Autowired
    public void setCityService(CityService cityService){
        this.cityService=cityService;
    }
    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    @RequestMapping(value = "/restaurants", method = RequestMethod.GET)
    public String list(Model model) {
        Iterable<Restaurant> restaurantList=restaurantService.listAllRestaurants();
                model.addAttribute("variableTexto","Hello world");
                model.addAttribute("restaurantList",restaurantList);
                return "restaurants";
    }

    @RequestMapping("/newRestaurant")
    String newRestaurant(Model model) {
        Iterable<Category> categories= categoryService.listAllCategorys();
        model.addAttribute("categories",categories);
        Iterable<City> cities= cityService.listAllCitys();
        model.addAttribute("cities",cities);

                return "newRestaurant";
    }

    @RequestMapping(value="/restaurant", method = RequestMethod.POST)
    String save(Restaurant restaurant) {
        restaurantService.saveRestaurant(restaurant);
        return "redirect:/restaurants";
    }

    @RequestMapping("/restaurant/{id}")
    String show(@PathVariable Integer id,Model model) {
        /*org.springframework.security.core.Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        org.springframework.security.core.userdetails.User u = (org.springframework.security.core.userdetails.User)auth.getPrincipal();
        com.example.proyecto.entities.User user = userService.findByUsername(u.getUsername());
        model.addAttribute("user",user);
        */



        Restaurant restaurant=restaurantService.getRestaurant(id);
        model.addAttribute("restaurant",restaurant);
        model.addAttribute("users",userService.listAllUsers());
        return "show";
    }

    @RequestMapping("/editRestaurant/{id}")
    String editRestaurant(@PathVariable Integer id,Model model) {

        Restaurant restaurant=restaurantService.getRestaurant(id);
        model.addAttribute("restaurant",restaurant);
        Iterable<Category> categories=categoryService.listAllCategorys();
        model.addAttribute("categories",categories);
        Iterable<City> cities=cityService.listAllCitys();
        model.addAttribute("cities",cities);
        return "editRestaurant";
    }

    @RequestMapping("/deleteRestaurant/{id}")
    String delete(@PathVariable Integer id)
    {
        restaurantService.deleteRestaurant(id);
               return "redirect:/restaurants";
    }


    /*
     @RequestMapping("/like/{id}")
     String like(@PathVariable Integer id) {
                Restaurant restaurant = restaurantService.getRestaurant(id);
                restaurant.getLike().setLikes(restaurant.getLike().getLikes()+1);
                restaurantService.saveRestaurant(restaurant);
                return "redirect:/restaurant/"+restaurant.getId();
    }
    */

}
