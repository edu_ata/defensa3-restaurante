package com.example.proyecto.repository;

import com.example.proyecto.entities.Likes;

import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface LikeRepository extends CrudRepository<Likes,Integer>{
}
