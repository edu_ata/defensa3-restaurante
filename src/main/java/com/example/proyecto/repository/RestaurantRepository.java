package com.example.proyecto.repository;

import com.example.proyecto.entities.Restaurant;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface RestaurantRepository extends CrudRepository<Restaurant,Integer>{

    @Query("select r from Restaurant r where r.name like %:name%")
    Iterable<Restaurant> getRestaurantsLikeName(@Param("name") String name);

    @Query("select r from Restaurant r where r.category like %:category%")
    Iterable<Restaurant> getRestaurantsLikeCategory(@Param("category") Integer category);

    @Query("select r from Restaurant r where r.city like %:city%")
    Iterable<Restaurant> getRestaurantsLikeCity(@Param("city") Integer city);
}
