package com.example.proyecto.repository;


import com.example.proyecto.entities.Comment;
import org.springframework.data.repository.CrudRepository;
import javax.transaction.Transactional;

@Transactional
public interface CommentRepository extends CrudRepository<Comment,Integer>{
}
